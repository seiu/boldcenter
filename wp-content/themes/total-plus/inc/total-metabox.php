<?php

/**
 *
 * @package Total Plus
 */
function total_plus_post_metabox($meta_boxes) {
    $prefix = 'total_plus_';
    $image_url = get_template_directory_uri() . '/inc/customizer/images/';

    $meta_boxes[] = array(
        'id' => 'total_post_setting',
        'title' => esc_html__('Post Setting', 'total-plus'),
        'post_types' => array('post', 'page', 'product', 'portfolio'),
        'context' => 'advanced',
        'priority' => 'high',
        'autosave' => true,
        'tabs' => array(
            'general-setting' => array(
                'label' => 'General Setting',
                'icon' => 'dashicons-admin-generic'
            ),
            'titlebar-setting' => array(
                'label' => 'Title Bar Setting',
                'icon' => 'dashicons-editor-kitchensink'
            ),
            'content-setting' => array(
                'label' => 'Content Setting',
                'icon' => 'dashicons-admin-page'
            ),
            'sidebar-setting' => array(
                'label' => 'Sidebar Setting',
                'icon' => 'dashicons-welcome-widgets-menus'
            )
        ),
        'tab_style' => 'left',
        'tab_wrapper' => true,
        'fields' => array(
            array(
                'name' => 'Hide Header',
                'id' => 'hide_header',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'columns' => 6,
                'tab' => 'general-setting'
            ),
            array(
                'name' => 'Hide Footer',
                'id' => 'hide_footer',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'columns' => 6,
                'tab' => 'general-setting'
            ),
            array(
                'name' => 'Page Background Options',
                'type' => 'group',
                'class' => 'background-group',
                'id' => 'page_background',
                'tab' => 'general-setting',
                'fields' => array(
                    array(
                        'id' => 'page_bg_color',
                        'type' => 'color'
                    ),
                    array(
                        'id' => 'page_bg_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false
                    ),
                    array(
                        'placeholder' => 'Background Repeat',
                        'id' => 'page_bg_repeat',
                        'type' => 'select_advanced',
                        'options' => array(
                            'no-repeat' => 'No Repeat',
                            'repeat' => 'Repeat All',
                            'repeat-x' => 'Repeat Horizontally',
                            'repeat-y' => 'Repeat Vertically'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('page_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Size',
                        'id' => 'page_bg_size',
                        'type' => 'select_advanced',
                        'options' => array(
                            'auto' => 'Auto',
                            'cover' => 'Cover',
                            'contain' => 'Contain'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('page_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Attachment',
                        'id' => 'page_bg_attachment',
                        'type' => 'select_advanced',
                        'options' => array(
                            'scroll' => 'Scroll',
                            'fixed' => 'Fixed'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('page_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Position',
                        'id' => 'page_bg_position',
                        'type' => 'select_advanced',
                        'options' => array(
                            'left top' => 'Left Top',
                            'left center' => 'Left Center',
                            'left bottom' => 'Left Bottom',
                            'center top' => 'Center Top',
                            'center center' => 'Center Center',
                            'center bottom' => 'Center Bottom',
                            'right top' => 'Right Top',
                            'right center' => 'Right Center',
                            'right bottom' => 'Right Bottom'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('page_bg_image', 0),
                        'columns' => 6
                    )
                )
            ),
            array(
                'name' => 'Page Text Color',
                'id' => 'page_text_color',
                'type' => 'color',
                'tab' => 'general-setting'
            ),
            array(
                'name' => 'Hide Titlebar',
                'id' => 'hide_titlebar',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'tab' => 'titlebar-setting'
            ),
            array(
                'name' => 'Sub Title',
                'id' => 'sub_title',
                'type' => 'text',
                'tab' => 'titlebar-setting',
                'attributes' => array(
                    'class' => 'widefat'
                )
            ),
            array(
                'name' => 'OverWrite Default Style',
                'label_description' => 'A set of settings will appear',
                'id' => 'page_overwrite_defaults',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'tab' => 'titlebar-setting'
            ),
            array(
                'name' => 'Titlebar Background Options',
                'type' => 'group',
                'class' => 'background-group',
                'tab' => 'titlebar-setting',
                'id' => 'titlebar_background',
                'hidden' => array('overwrite_defaults', false),
                'fields' => array(
                    array(
                        'id' => 'titlebar_bg_color',
                        'type' => 'color'
                    ),
                    array(
                        'id' => 'titlebar_bg_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false
                    ),
                    array(
                        'placeholder' => 'Background Repeat',
                        'id' => 'titlebar_bg_repeat',
                        'type' => 'select_advanced',
                        'options' => array(
                            'no-repeat' => 'No Repeat',
                            'repeat' => 'Repeat All',
                            'repeat-x' => 'Repeat Horizontally',
                            'repeat-y' => 'Repeat Vertically'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('titlebar_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Size',
                        'id' => 'titlebar_bg_size',
                        'type' => 'select_advanced',
                        'options' => array(
                            'inherit' => 'Inherit',
                            'cover' => 'Cover',
                            'contain' => 'Contain'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('titlebar_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Attachment',
                        'id' => 'titlebar_bg_attachment',
                        'type' => 'select_advanced',
                        'options' => array(
                            'inherit' => 'Inherit',
                            'fixed' => 'Fixed',
                            'scroll' => 'Scroll'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('titlebar_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'placeholder' => 'Background Position',
                        'id' => 'titlebar_bg_position',
                        'type' => 'select_advanced',
                        'options' => array(
                            'left top' => 'Left Top',
                            'left center' => 'Left Center',
                            'left bottom' => 'Left Bottom',
                            'center top' => 'Center Top',
                            'center center' => 'Center Center',
                            'center bottom' => 'Center Bottom',
                            'right top' => 'Right Top',
                            'right center' => 'Right Center',
                            'right bottom' => 'Right Bottom'
                        ),
                        'js_options' => array(
                            'width' > '500px',
                            'allowClear' => false
                        ),
                        'hidden' => array('titlebar_bg_image', 0),
                        'columns' => 6
                    ),
                    array(
                        'name' => 'Overlay Background Color',
                        'id' => 'overlay_bg_color',
                        'type' => 'color',
                        'alpha_channel' => true,
                        'hidden' => array('titlebar_bg_image', 0)
                    ),
                    array(
                        'name' => 'Enable Parallax Effect',
                        'id' => 'enable_parallax_effect',
                        'type' => 'switch',
                        'style' => 'square',
                        'on_label' => 'Yes',
                        'off_label' => 'No',
                        'std' => 0,
                        'class' => 'switch no-margin',
                        'hidden' => array('titlebar_bg_image', 0)
                    )
                )
            ),
            array(
                'name' => 'Text Color',
                'id' => 'titlebar_color',
                'type' => 'color',
                'hidden' => array('overwrite_defaults', false),
                'tab' => 'titlebar-setting'
            ),
            array(
                'name' => 'Top Bottom Padding',
                'id' => 'titlebar_padding',
                'type' => 'slider',
                'suffix' => ' px',
                'js_options' => array(
                    'min' => 0,
                    'max' => 200,
                    'step' => 5
                ),
                'std' => 80,
                'hidden' => array('overwrite_defaults', false),
                'tab' => 'titlebar-setting'
            ),
            array(
                'name' => 'Display Title in Content',
                'id' => 'content_display_title',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'tab' => 'content-setting'
            ),
            array(
                'name' => 'Display Featured Image in Content',
                'id' => 'content_display_featured_image',
                'type' => 'switch',
                'style' => 'square',
                'on_label' => 'Yes',
                'off_label' => 'No',
                'std' => 0,
                'tab' => 'content-setting'
            ),
            array(
                'name' => 'Content Width',
                'id' => 'content_width',
                'type' => 'radio',
                'options' => array(
                    'container' => 'Inside Container',
                    'full-width' => 'Full Width'
                ),
                'inline' => true,
                'std' => 'container',
                'tab' => 'content-setting'
            ),
            array(
                'id' => 'sidebar_layout',
                'type' => 'image_select',
                'name' => 'Sidebar Layout',
                'options' => array(
                    'default-sidebar' => $image_url . 'default.png',
                    'right-sidebar' => $image_url . 'right-sidebar.png',
                    'left-sidebar' => $image_url . 'left-sidebar.png',
                    'no-sidebar' => $image_url . 'no-sidebar.png',
                    'no-sidebar-narrow' => $image_url . 'no-sidebar-narrow.png'
                ),
                'std' => 'default-sidebar',
                'tab' => 'sidebar-setting'
            ),
            array(
                'name' => 'Left Sidebar',
                'id' => 'left_sidebar',
                'type' => 'sidebar',
                'field_type' => 'select_advanced',
                'placeholder' => 'Select a sidebar',
                'columns' => 6,
                'tab' => 'sidebar-setting'
            ),
            array(
                'name' => 'Right Sidebar',
                'id' => 'right_sidebar',
                'type' => 'sidebar',
                'field_type' => 'select_advanced',
                'placeholder' => 'Select a sidebar',
                'columns' => 6,
                'tab' => 'sidebar-setting'
            )
        )
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'total_plus_post_metabox');
